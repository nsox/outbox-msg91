require 'outbox'

module Outbox
  module Msg91
    require 'outbox/msg91/version'
    require 'outbox/msg91/client'
  end

  Messages::SMS.register_client_alias(:msg91, Outbox::Msg91::Client)
end
