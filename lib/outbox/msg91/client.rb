module Outbox
  module Msg91
    # Uses MSG91s php API client  to deliver SMS messages.
    #
    #   Outbox::Messages::SMS.default_client(
    #     :msg91,
    #     authkey = "xxxxxxxxxxxxxxxxxxx")
    #   sms = Outbox::Messages::SMS.new(
    #     from:  = "ABCDEFG"  <6 digit code>   
    #     body: 'Hello World'
    #   )
    #   sms.deliver('+15552224444')
    class Client < Outbox::Clients::Base
      attr_reader :auth_key, :senderid

      def initialize(settings = nil)
        super

        options = @settings.dup
        p @options

        # 4 indicates transactional sms
        @route_code = 4
        @authkey = options[:authkey]
      end


      def full_path(url, path, params)
        encoded_params = URI.encode_www_form(params)
        params_string = [path, encoded_params].join("?")
        URI.parse(url + params_string)
      end

      #MSG91 API Documented at https://api.msg91.com/apidoc/textsms/send-sms.php
      def deliver(sms)
        params = {:authkey => @authkey, :mobiles => sms.to, :message => sms.body , :sender => sms.from ,:route => @route_code, :response => "json"}
        url = "http://api.msg91.com"
        uri = full_path(url, '/api/sendhttp.php', params)
        #p uri
        response = Net::HTTP.get(uri)
      end

    end
  end
end
