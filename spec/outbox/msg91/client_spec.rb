require 'spec_helper'

describe Outbox::Msg91::Client do
  describe '.new' do
  	it 'expects configuration fot the Msg91 API' do
  		# client = Outbox::Msg91::Client(
		  # 	:authkey "ABCDEFG"
		  # )
		  client = Outbox::Messages::SMS.default_client(
 			 :msg91,
 		   authkey: "ABCDEFG"
 			)	
		 expect(client.authkey).not_to be_empty
	  end
	end
  describe '#deliver' do
   	before do
		 	# @client = Outbox::Msg91::Client(
		  # 	:authkey "ABCDEFG"
		  # )
		  @client = Outbox::Messages::SMS.default_client(
 			 :msg91,
 		   authkey: "ABCDEFG"
 			)
		  @sms = Outbox::Messages::SMS.new do
        to '+919876543210'
        from 'SENDER'
        body 'Hello world.'
      end
    end
    it 'delivers the SMS' do
    	response  = @client.deliver(@sms)
    	parsed_body = JSON.parse(response.body)
    	expect(parsed_body).not_to be_empty
    end	
  end
end
