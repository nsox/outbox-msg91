Outbox::Msg91
==============

[![Gem Version](https://badge.fury.io/rb/outbox-msg91.png)](http://badge.fury.io/rb/outbox-msg91)

MSG91 SMS API wrapper for [Outbox](https://github.com/localmed/outbox).

## Installation

Add this line to your application's Gemfile:

``` ruby
gem 'outbox-msg91'
```

And then execute:

``` bash
$ bundle
```

Or install it yourself as:

``` bash
$ gem install outbox-msg91
```

## Usage

Configure Outbox to use Outbox::Msg91 as the default SMS client:

``` ruby
Outbox::Messages::SMS.default_client(
  :msg91,
  authkey = "xxxxxxxxxxxxxxxxxxx"
)
```

SMS messages will now use the Msg91 Ap for devlivery:

``` ruby
sms = Outbox::Messages::SMS.new(
  from: "ABCDEFG", <6 digit sender id>
  body: 'Hello World',
 )
sms.deliver('+15552224444')
```

## Contributing

1. Fork it ( https://github.com/nsox/outbox-msg1/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request