# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'outbox/msg91/version'

Gem::Specification.new do |spec|
  spec.name          = 'outbox-msg91'
  spec.version       = Outbox::Msg91::VERSION
  spec.authors       = ['Rabi Cherian']
  spec.email         = ['rabi@nsox.tech']
  spec.summary       = %q{Outbox SMS client for MSG91.}
  spec.description   = %q{MSG91 API wrapper for Outbox, a generic interface for sending sms notificatons.}
  spec.homepage      = 'https://bitbucket.org/nsox/outbox-msg91'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'outbox', '~> 0.2'
  spec.add_development_dependency 'bundler', '~> 1.6'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec'
end
